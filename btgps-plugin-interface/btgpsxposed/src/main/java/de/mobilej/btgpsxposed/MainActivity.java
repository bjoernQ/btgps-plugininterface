package de.mobilej.btgpsxposed;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;


public class MainActivity extends Activity {

    public static final String PREF_NO_FALLBACK = "no_fallback";
    public static final String PREF_NAME = "myprefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences prefs = getSharedPreferences(PREF_NAME, Context.MODE_MULTI_PROCESS | Context.MODE_WORLD_READABLE);


        CheckBox checkBox = (CheckBox) findViewById(R.id.chkbox_no_fallback);
        checkBox.setChecked(prefs.getBoolean(PREF_NO_FALLBACK, false));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                prefs
                        .edit()
                        .putBoolean(PREF_NO_FALLBACK, b)
                        .apply();
            }
        });
    }


}
