package de.mobilej.btgpsfakelocation;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.mobilej.btgps.plugin.IBtgpsPlugin;

/**
 * The plugin service.
 * <p>
 * adb forward tcp:8888 tcp:8888
 * <p>
 * <p>
 * Created by bjoern on 09.11.14.
 */
public class PluginService extends Service {

    private static ServerSocket serverSocket;
    private static float lat;
    private static float lon;

    public class BtgpsPluginImpl extends IBtgpsPlugin.Stub {

        private RemoteViews rv;

        private String text;

        @Override
        public String beforeParse(String nmeaSentence) throws RemoteException {
            return nmeaSentence;
        }

        @Override
        public RemoteViews getMainScreenWidget() throws RemoteException {

            rv = new RemoteViews(getApplicationContext().getPackageName(),
                    R.layout.plugin);
            if (text != null) {
                rv.setTextViewText(R.id.plugin_text, text);
            }
            return rv;
        }

        @Override
        public boolean setGpsInfo(int[] prns, float[] snrs, float[] elevations, float[] azimuths,
                                  int ephemerisMask, int almanacMask, int usedInFixMask) throws RemoteException {
            return false;
        }

        @Override
        public boolean setLocation(Location loc) throws RemoteException {
            text = "" + loc;
            return false;
        }

        @Override
        public boolean setStatus(int status) throws RemoteException {
            return false;
        }

        @Override
        public boolean setup() throws RemoteException {
            return false;
        }

        @Override
        public boolean useAlternateSource() throws RemoteException {
            return true;
        }

        @Override
        public boolean noMockLocations() throws RemoteException {
            return false;
        }

        @Override
        public String getNMEA() throws RemoteException {
            Log.d(TAG, "getNMEA");
            SystemClock.sleep(1000);

            DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance(Locale.US);

            String latSign = PluginService.lat < 0 ? "S" : "N";
            int latDeg = (int) PluginService.lat;
            float latMins = (((PluginService.lat - latDeg) / 100f) * 60f) * 100f;
            String latStr = new DecimalFormat("00", symbols).format(Math.abs(latDeg)) + new DecimalFormat("00.00", symbols).format(Math.abs(latMins));

            String lonSign = PluginService.lon < 0 ? "W" : "E";
            int lonDeg = (int) PluginService.lon;
            float lonMins = (((PluginService.lon - lonDeg) / 100f) * 60f) * 100f;
            String lonStr = new DecimalFormat("000", symbols).format(Math.abs(lonDeg)) + new DecimalFormat("00.00", symbols).format(Math.abs(lonMins));

            Date d = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime();
            final SimpleDateFormat hHmmss = new SimpleDateFormat("HHmmss", Locale.US);
            hHmmss.setTimeZone(TimeZone.getTimeZone("UTC"));
            String time = hHmmss.format(d);
            final SimpleDateFormat ddMMyy = new SimpleDateFormat("ddMMyy", Locale.US);
            ddMMyy.setTimeZone(TimeZone.getTimeZone("UTC"));
            String date = ddMMyy.format(d);
            System.err.println("$GPRMC," + time + ",A," + latStr + "," + latSign + "," + lonStr + "," + lonSign + ",000.0,360.0," + date + ",011.3,E*62");
            return "$GPRMC," + time + ",A," + latStr + "," + latSign + "," + lonStr + "," + lonSign + ",000.0,360.0," + date + ",011.3,E*62";
        }

        @Override
        public boolean tearDown() throws RemoteException {
            Log.d(TAG, "tearDown");
            return false;
        }
    }

    private static final String TAG = "plugin_example";

    private IBinder mBinder = new BtgpsPluginImpl();

    @Override
    public IBinder onBind(Intent intent) {
        try {
            if (PluginService.serverSocket == null) {
                PluginService.serverSocket = new ServerSocket(8888);
                new MyThread().start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mBinder;
    }

    private void handleUrl(String url, PrintWriter pw) {
        System.err.println("" + url);

        // e.g. /setlatlng?latlng=LatLng(50.87011,%204.39453) ... parse latlng set location
        if (url.startsWith("/setlatlng")) {
            String latlng = url.substring(url.indexOf("latlng=LatLng(") + 14);
            latlng = latlng.substring(0, latlng.indexOf(")"));
            latlng = latlng.replace("%20", "");
            String[] latlngArray = latlng.split("\\,");
            lat = Float.parseFloat(latlngArray[0]);
            lon = Float.parseFloat(latlngArray[1]);

            System.err.println("" + lat + " " + lon);
            pw.println("HTTP/1.0 200 OK");
            pw.println("Content-Type: text/html");
            pw.println();
            pw.println("Empty");
            return;
        } else if (url.equals("/favicon.ico")) {
            pw.println("HTTP/1.0 404 NOT FOUND");
            pw.println();
            return;
        }


        // e.g. "/page.html"
        pw.println("HTTP/1.0 200 OK");
        pw.println("Content-Type: text/html");
        pw.println();
        pw.println("\n" +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<title>BTGPS Fake Location</title>\n" +
                "\t<meta charset=\"utf-8\" />\n" +
                "\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "\n" +
                "\t<link rel=\"stylesheet\" href=\"https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.css\" />\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div id=\"mapid\" style=\"width: 800px; height: 800px\"></div>\n" +
                "\n" +
                "\t<script src=\"https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js\"></script>\n" +
                "\t<script>\n" +
                "\n" +
                "\t\tvar mymap = L.map('mapid').setView([50.93222, 6.95433], 13);\n" +
                "\n" +
                "\t\tL.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {\n" +
                "\t\t\tmaxZoom: 18,\n" +
                "\t\t\tattribution: 'Map data &copy; <a href=\"http://openstreetmap.org\">OpenStreetMap</a> contributors, ' +\n" +
                "\t\t\t\t'<a href=\"http://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>, ' +\n" +
                "\t\t\t\t'Imagery © <a href=\"http://mapbox.com\">Mapbox</a>',\n" +
                "\t\t\tid: 'mapbox.streets'\n" +
                "\t\t}).addTo(mymap);\n" +
                "\n" +
                "\n" +
                "var myPos = L.marker([51.5, -0.09]).addTo(mymap);\n\n" +
                "\n" +
                "\t\tfunction onMapClick(e) {\n" +
                "myPos.setLatLng(e.latlng);\n" +
                "var request = new XMLHttpRequest();\n" +
                "request.open(\"GET\",\"setlatlng?latlng=\"+e.latlng.toString());" +
                "request.send();" +
                "\t\t}\n" +
                "\n" +
                "\t\tmymap.on('click', onMapClick);\n" +
                "\n" +
                "\t</script>\n" +
                "</body>\n" +
                "</html>\n");
    }

    private class MyThread extends Thread {
        @Override
        public void run() {
            try {
                Socket socket = serverSocket.accept();
                new MyThread().start();

                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

                String line = null;
                while ((line = br.readLine()) != null) {
                    if (line.equals("")) {
                        break;
                    }

                    if (line.startsWith("GET ")) {
                        String url = line.substring(4);
                        url = url.substring(0, url.indexOf(" "));

                        handleUrl(url, pw);
                        pw.flush();
                    }
                }

                br.close();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
